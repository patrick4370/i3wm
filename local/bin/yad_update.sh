#!/bin/bash -x
#===============================================================================
#
#          FILE: yad_update.sh
#
#         USAGE: ./yad_update.sh
#
#   DESCRIPTION: 
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Patrick Heffernan (PH), patrick4370@bigpond.com
#  ORGANIZATION: Your Dog Needs a Coat
#       CREATED: 13/07/22 12:14:25
#      REVISION:  ---
#===============================================================================

pacupdate=$(/usr/bin/checkupdates 2>/dev/null | wc -l)
aurupdate=$(/usr/bin/pikaur -Qua 2>/dev/null | wc -l)

[[ ($pacupdate -eq 0) && ($aurupdate -eq 0) ]] && exit

yad --notification --image="distributor-logo-archlinux" --text "There are ${pacupdate} + ${aurupdate} update's" --command "alacritty --class pikaur -e pikaur -Syu"
kill -USR1 $YAD_PID
exit 0;

