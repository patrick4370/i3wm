# i3wm Setup

This is my setup of i3wm.
I will be updating this repository often as I update my setup. I am running Arch Linux. To clone just do on the command line

`git clone https://gitlab.com/patrick4370/i3wm.git`

#### Autostart Items 

| Command | Package Name |
|:--------|:-------------|
| blueman-applet | blueman |
| clipmenud | clipmenu |
| dunst | dunst |
| ibus-daemon | ibus |
| lxpolkit | lxsession-gtk3 |
| mpDris2 | mpdris2-git |
| nm-applet | network-manager-applet |
| onedrivegui | onedrivegui-git |
| udiskie | udiskie |
| unclutter | unclutter |
| vgrive | vgrive |

#### Scratchpads

I use a number of scratchpads. 
- General purpose alacritty window
- Newsboat news reader
- Ranger file manager
- Bpytop system monitor

I also have a scratchpad window set when a dunst notification comes up informing me of updates. 
I can middle click to have the floating window appear and run pacman.
